// import './App.css';
import { Formik, Field, Form } from "formik";

function Input(props) {
  return (
    <tr><td><label>{props.label}</label></td><td><Field name={props.id} type={props.type} maxlength="30" /></td></tr>
  );
}

export default Input;
