// import './App.css';
import Button from '@material-ui/core/Button';
import { Formik, Field, Form } from "formik";
import Input from "../Input/input"
import { connect } from 'react-redux';
import { compose } from 'redux';
import { setData, addData } from '../../redux/actions';

function FormSection(props) {
    const newadd = (values) => {
		console.log("values",values)
		var {city,State,Population} = values;
		if (
			city !== '' &&
			State !== '' &&
			Population !== '' &&
			city !== null &&
			State !== null &&
			Population !== null
		) {
			var obj = {
				city,
				State,
				Population: parseInt(Population)
			};
			// document.getElementById('city').value = '';
			// document.getElementById('State').value = '';
			// document.getElementById('Population').value = '';
			props.addData(obj);
		} else {
			alert('fill the empty text box');
			return;
		}
	};
    return (
    
    <Formik
        initialValues={{ city:"",State:"",Population:"" }}
        onSubmit={async values => {
          // await new Promise(resolve => setTimeout(resolve, 500));
          newadd(values)
        }}
      >

        <Form>
        <table className="inputTable">
            <tbody>
        <Input type="text" label="US City" id="city" />
        <Input type="text" label="State" id="State" />
        <Input type="number" label="Population" id="Population" />
        </tbody>
			</table>
            <br/><br/>
        <Button style={{marginLeft:"50px"}} variant="outlined" type="submit">Add</Button>
        </Form>
      </Formik>
      );
  }
  const mapStateToProps = (State, ownProps) => {
	return { userData: State.data };
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		setData: (data) => dispatch(setData(data)),
    addData: (data) => dispatch(addData(data))
	};
};

const reduxWrapper = connect(mapStateToProps, mapDispatchToProps);

export default compose(reduxWrapper)(FormSection);
  