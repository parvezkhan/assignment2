// import './App.css';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { setData, addData } from '../../redux/actions';
function Table(props) {
  return (
    <table>
    <thead>
        <tr>
            <th
                onClick={() => {
                    var data2 = props.userData.data.sort((a, b) => a.city.localeCompare(b.city));
                    props.setData(data2);
                }}
            >
                US City
            </th>
            <th> State </th>
            <th
                onClick={() => {
                    var data2 = props.userData.data.sort((a, b) => a.Population - b.Population);
                    props.setData(data2);
                }}
            >
                Population
            </th>
        </tr>
    </thead>
    <tbody>
        {props.userData.data.map((item) => {
            return (
                <tr>
                    <td class="US City"> {item.city} </td> <td class="State"> {item.State} </td>
                    <td class="Population"> {item.Population} </td>
                </tr>
            );
        })}
    </tbody>
</table>
  );
}

const mapStateToProps = (State, ownProps) => {
	return { userData: State.data };
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		setData: (data) => dispatch(setData(data)),
    addData: (data) => dispatch(addData(data))
	};
};

const reduxWrapper = connect(mapStateToProps, mapDispatchToProps);

export default compose(reduxWrapper)(Table);
