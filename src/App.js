import './App.css';
import React, { useState } from 'react';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { setData, addData } from './redux/actions';
import From from "./Component/Form/Form"
import Table from "./Component/Table"
function App(props) {

	return (
		<div className="App">
			<From />
			<br /> <br />
			<Table/>
		</div>
	);
}

// const mapStateToProps = (State, ownProps) => {
// 	return { userData: State.data };
// };

// const mapDispatchToProps = (dispatch, ownProps) => {
// 	return {
// 		setData: (data) => dispatch(setData(data)),
//     addData: (data) => dispatch(addData(data))
// 	};
// };

// const reduxWrapper = connect(mapStateToProps, mapDispatchToProps);

// export default compose(reduxWrapper)(App);
export default App;
