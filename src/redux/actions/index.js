import * as types from "../types";

export const setData = (newItem) => {
  return { type: types.SET_DATA, newItem };
};


export const addData = (newItem) => {
  return { type: types.ADD_DATA, newItem };
};