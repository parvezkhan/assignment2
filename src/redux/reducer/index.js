import * as types from "../types";
const initialState = {
  data: [],
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_DATA:
      return { ...state, data: [...state.data, action.newItem] };
      case types.SET_DATA:
        return { ...state, data: action.newItem };
    default:
      return state;
  }
};

export default authReducer;
