#stage 1
FROM node:14.15-alpine3.12 as node
WORKDIR /assignment
ADD . .
RUN npm install
RUN npm run build
#stage 2
FROM nginx:1.19.6-alpine
COPY --from=node /assignment/build/  /usr/share/nginx/html/
RUN rm /etc/nginx/conf.d/default.conf
COPY --from=node /assignment/default.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
